import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IdGeneratorService {

  constructor() { }

  generateId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c, r) => {
      // tslint:disable-next-line:no-bitwise
      return ('x' === c ? (r = Math.random() * 16 | 0) : (r & 0x3 | 0x8)).toString(16);
    });
  }
}
