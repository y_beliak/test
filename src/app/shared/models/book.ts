export interface Book {
  id?: string;
  title: string | null;
  author: string | null;
  category: string | null;
  year: number | null;
}
