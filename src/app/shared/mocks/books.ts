import { Book } from '../models/book';

export const books: Book[] = [
  {
    id: '6d0709f9-01de-451c-bf68-cde667813e0b',
    title: 'The Testaments',
    author: 'Margaret Atwood',
    category: 'Fiction',
    year: 2019,
  },
  {
    id: '997e5483-4e2e-409f-bf97-c790b56b4519',
    title: '1984',
    author: 'George Orwell',
    category: 'Fiction',
    year: 2003,
  },
  {
    id: '19a3e257-a8cb-49c1-ba1e-3b6f14984d3c',
    title: 'Lord of the Flies',
    author: 'William Golding',
    category: 'Fiction',
    year: 1999,
  },
  {
    id: '40371e9b-38e3-4692-b9e8-592b9eccf2d0',
    title: 'Harry potter and the philosopher\'s stone',
    author: 'Joanne Rowling',
    category: 'Fantasy',
    year: 1997,
  },
  {
    id: '97ceb42b-b1a3-443f-be60-8948af300bd9',
    title: 'A Song of Ice and Fire',
    author: 'George R.R. Martin',
    category: 'Fantasy',
    year: 1996,
  },
  {
    id: '3c0a5398-efa8-4890-b93f-2ea52ce800f6',
    title: 'The Silent Patient',
    author: 'Alex Michaelides',
    category: 'Mystery & thriller',
    year: 2019,
  },
];
