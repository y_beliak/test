import { Book } from '../../shared/models/book';

export class AddBook {
  static readonly type = '[Books] Add';
  constructor(public payload: Book) {}
}

export class RemoveBook {
  static readonly type = '[Books] Remove';
  constructor(public payload: string) {}
}

export class UpdateBook {
  static readonly type = '[Books] Update';
  constructor(public payload: Book) {}
}

export  class SelectBook {
  static readonly type = '[Books] Select';
  constructor(public payload: string) {}
}

export class ResetSelectedBook {
  static readonly type = '[Books] ResetSelectedBook';
  constructor() {}
}

export class SelectBookCategory {
  static readonly type = '[Books] SelectCategory';
  constructor(public payload: string) {}
}
