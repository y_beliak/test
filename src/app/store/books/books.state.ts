import { Action, Selector, State, StateContext } from '@ngxs/store';
import { append, patch, removeItem, updateItem } from '@ngxs/store/operators';

import { books } from '../../shared/mocks/books';
import { Book } from '../../shared/models/book';
import { AddBook, RemoveBook, ResetSelectedBook, SelectBook, SelectBookCategory, UpdateBook } from './books.actions';

export interface BooksStateModel {
  booksList: Book[];
  booksCategories: string[];
  selectedCategory?: string;
  selectedBook?: Book;
}

@State<BooksStateModel>({
  name: 'books',
  defaults: {
    booksList: books,
    booksCategories: Array.from(new Set(books.map(book => book.category)))
  }
})
export class BooksState {
  @Selector()
  static books(state: BooksStateModel) {
    return state.booksList;
  }

  @Selector()
  static bookCategories(state: BooksStateModel) {
    return state.booksCategories;
  }

  @Selector()
  static selectedCategory(state: BooksStateModel) {
    return state.selectedCategory;
  }

  @Selector()
  static selectedBook(state: BooksStateModel) {
    return state.selectedBook;
  }

  @Action(AddBook)
  add(ctx: StateContext<BooksStateModel>, {payload}: AddBook) {
    ctx.setState(
      patch({
        booksList: append([payload])
      })
    );
  }

  @Action(RemoveBook)
  remove(ctx: StateContext<BooksStateModel>, {payload}: RemoveBook) {
    ctx.setState(
      patch({
        booksList: removeItem<Book>(book => book.id === payload)
      })
    );
  }

  @Action(UpdateBook)
  update(ctx: StateContext<BooksStateModel>, {payload}: UpdateBook) {
    ctx.setState(
      patch({
        booksList: updateItem<Book>(book => book.id === payload.id, payload)
      }));
  }

  @Action(SelectBook)
  select({getState, patchState}: StateContext<BooksStateModel>, {payload}: SelectBook) {
    const state = getState();
    patchState({
      selectedBook: {...state.booksList.filter(book => book.id === payload)}[0]
    });
  }

  @Action(ResetSelectedBook)
  reset({patchState}: StateContext<BooksStateModel>) {
    patchState({
      selectedBook: null
    });
  }

  @Action(SelectBookCategory)
  selectCategory({patchState}: StateContext<BooksStateModel>, {payload}: SelectBookCategory) {
    patchState({
      selectedCategory: payload
    });
  }

}
