import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksTableComponent } from './components/book-container/books-table/books-table.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { materialModules } from './shared/material.imports';
import { BookFormComponent } from './components/book-container/book-form/book-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';
import { BooksState } from './store/books/books.state';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { CategoryFilterComponent } from './components/book-container/category-filter/category-filter.component';
import { BookContainerComponent } from './components/book-container/book-container.component';

@NgModule({
  declarations: [
    AppComponent,
    BooksTableComponent,
    BookFormComponent,
    CategoryFilterComponent,
    BookContainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    materialModules,
    NgxsModule.forRoot([
      BooksState
    ]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
