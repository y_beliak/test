import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective } from '@angular/forms';
import { Store } from '@ngxs/store';

import { Book } from '../../../shared/models/book';
import { IdGeneratorService } from '../../../shared/id-generator.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit, OnChanges {

  form: FormGroup;
  @Input() selectedBook: Book;
  @Input() bookCategories: string[];
  @Output() resetSelectedBook: EventEmitter<void> = new EventEmitter<void>();
  @Output() bookToAdd: EventEmitter<Book> = new EventEmitter<Book>();
  @Output() bookToUpdate: EventEmitter<Book> = new EventEmitter<Book>();
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private store: Store,
    private idGenerator: IdGeneratorService
  ) {
  }

  ngOnInit(): void {
    this.createInitialForm();
  }

  ngOnChanges(): void {
    if (this.selectedBook) {
      this.fillForm(this.selectedBook);
    }
  }

  createInitialForm() {
    this.form = new FormGroup({
      title: new FormControl(null),
      author: new FormControl(null),
      year: new FormControl(null),
      category: new FormControl(null),
    });
  }

  submit() {
    if (!this.selectedBook) {
      this.addBook();
    } else {
      this.editBook();
    }
    this.clearForm();
  }

  clearForm() {
    this.formGroupDirective.resetForm();
    this.resetSelectedBook.emit();
  }

  fillForm(book: Book) {
    this.form.patchValue({
      ...book
    });
  }

  addBook() {
    const newBook: Book = this.form.value;
    newBook.id = this.idGenerator.generateId();
    this.bookToAdd.emit(newBook);
  }

  editBook() {
    const bookToBeUpdated = this.form.value;
    bookToBeUpdated.id = this.selectedBook.id;
    this.bookToUpdate.emit(bookToBeUpdated);
  }
}
