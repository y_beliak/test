import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Book } from '../../../shared/models/book';

@Component({
  selector: 'app-books-table',
  templateUrl: './books-table.component.html',
  styleUrls: ['./books-table.component.scss']
})
export class BooksTableComponent implements OnInit, OnChanges {
  displayedColumns: string[] = ['title', 'author', 'year', 'category', 'action'];
  dataSource: MatTableDataSource<Book>;
  @Input() bookList: Book[];
  @Input() selectedCategory: string;
  @Output() bookSelected: EventEmitter<string> = new EventEmitter<string>();
  @Output() bookToRemove: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(): void {
    this.dataSource = new MatTableDataSource(this.bookList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.applyFilter(this.selectedCategory);
  }

  applyFilter(category: string) {
    this.dataSource.filter = category;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selectBook(id: any) {
    this.bookSelected.emit(id);
  }

  removeBook(id: any) {
    this.bookToRemove.emit(id);
  }
}
