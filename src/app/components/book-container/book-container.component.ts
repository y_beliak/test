import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { BooksState } from '../../store/books/books.state';

import { Book } from '../../shared/models/book';
import { AddBook, RemoveBook, ResetSelectedBook, SelectBook, SelectBookCategory, UpdateBook } from '../../store/books/books.actions';

@Component({
  selector: 'app-book-container',
  templateUrl: './book-container.component.html',
  styleUrls: ['./book-container.component.scss']
})
export class BookContainerComponent implements OnInit {
  @Select(BooksState.books) bookList$: Observable<Book[]>;
  @Select(BooksState.bookCategories) bookCategories$: Observable<string []>;
  @Select(BooksState.selectedBook) selectedBook$: Observable<Book>;
  @Select(BooksState.selectedCategory) selectedCategory$: Observable<string>;

  constructor(private store: Store) {
  }

  ngOnInit(): void {
  }

  removeBook(id: string) {
    this.store.dispatch(new RemoveBook(id));
  }

  editBook(dataToUpdate: Book) {
    this.store.dispatch(new UpdateBook(dataToUpdate));
  }

  addBook(newBook: Book) {
    this.store.dispatch(new AddBook(newBook));
  }

  selectCategory(category: string) {
    this.store.dispatch(new SelectBookCategory(category));
  }

  selectBook(id: string) {
    this.store.dispatch(new SelectBook(id));
  }

  resetSelectBook() {
    this.store.dispatch(new ResetSelectedBook());
  }
}
